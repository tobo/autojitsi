#AutoIt3Wrapper_Au3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -w 7

;#AutoIt3Wrapper_UseX64=n ; If target application is running as 32 bit code
;#AutoIt3Wrapper_UseX64=y ; If target application is running as 64 bit code

#include "includes\UIA_Constants.au3" ; Can be copied from UIASpy Includes folder
;#include "UIA_Functions.au3" ; Can be copied from UIASpy Includes folder
;#include "UIA_SafeArray.au3" ; Can be copied from UIASpy Includes folder
;#include "UIA_Variant.au3" ; Can be copied from UIASpy Includes folder
#include <Windowsconstants.au3>
#include <WinAPIGdi.au3>
#include <ColorConstants.au3>
#include <GUIConstantsEx.au3>
#include <FontConstants.au3>
#include <WinAPISysWin.au3>

; =========================================================
; Auto Jitsi/Jitsi Remote
;
; Version 1.2
; initially established for privacyweek.at
;
; (c) @tobo@chaos.social
;
; CC-BY-SA-NC 4.0
;
; =========================================================
global $version="1.3"
global $debuglevel=1


Opt( "MustDeclareVars", 1 )

global $jitsipath="c:\jitsi-electron\Jitsi Meet.exe"
global $commandfile="autojitsi.command.ini"

global $_appstr_cf_go="GO"
global $_appstr_cf_enterconference="Konferenz beitreten"
global $_appstr_cf_confuser="Bitte geben Sie hier Ihren Namen ein"

global $_appstr_rc_permit="Erlauben"
global $_appstr_rc_share="Teilen"
global $_appstr_toolb_microphonemute="Mikrofon aktivieren / deaktivieren"
global $_appstr_toolb_screenshareon="Bildschirmfreigabe ein-/ausschalten"
global $_appstr_toolb_videomute="„Video stummschalten“ ein-/ausschalten"


Global $GLOBAL_MAIN_GUI, $Win_Min_ResizeX = 145, $Win_Min_ResizeY = 45

#region Example
global $boxwidth=300
global $boxheight=150
global $Form1 = GUICreate("Jitsi Remote " & $version, $boxwidth, $boxheight, @DesktopWidth-$boxwidth, @desktopheight-$boxheight, bitor($DS_MODALFRAME,$DS_SETFOREGROUND),$WS_EX_TOPMOST)
GUISetBkColor(0x282828, $Form1)
global $Label_1 = GUICtrlCreateLabel("Startup", 15, 10, $boxwidth-30, 50,1)
GUICtrlSetFont($Label_1,24,$FW_BOLD)
GUICtrlSetColor($Label_1,$COLOR_WHITE)
global $Label_2 = GUICtrlCreateLabel("", 15, 70, $boxwidth-30, 40,1)
GUICtrlSetFont($Label_2,18,$FW_BOLD)
GUICtrlSetColor($Label_2,$COLOR_WHITE)

;_GUI_EnableDragAndResize($Form1,300, 150)
; global $Button1 = GUICtrlCreateButton("Exit", 90, 60, 120, 30)
; GUICtrlSetResizing(-1, 768 + 8 + 128)


GUISetState(@SW_SHOW)



;msleep(10000)
;startconference("https://talks.privacyweek.at/test123")
;msleep(10000)
;enterconference()

    ; Create UI Automation object
    global $oUIAutomation = ObjCreateInterface( $sCLSID_CUIAutomation8, $sIID_IUIAutomation3, $dtag_IUIAutomation3 )
    If Not IsObj( $oUIAutomation ) Then exiterror("$oUIAutomation ERR"  )
    logger( "$oUIAutomation OK" & @CRLF )

    ; Get Desktop element
    global $pDesktop, $oDesktop
    $oUIAutomation.GetRootElement( $pDesktop )
    $oDesktop = ObjCreateInterface( $pDesktop, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
    If Not IsObj( $oDesktop ) Then exiterror( "$oDesktop ERR"  )
    logger( "$oDesktop OK" & @CRLF )


global $confuser=""
global $confmic=""
global $confcam=""
global $confsvrpfx=""

logger("*** startup autojitsi " & $version & @crlf,1)

while 1=1
  $confuser=IniRead($commandfile,"settings","user","")
  $confmic=IniRead($commandfile,"settings","microphone","")
  $confcam=IniRead($commandfile,"settings","camera","")
  checkcommand()
  enterconference(true)
  configurescreensharing(true)
  permitremotecontrol(true)

  checkjitsi()
  sleep(2000)
WEnd

func checkjitsi()
  local $jitsihdl
  logger("checkjitsi: ")
  $jitsihdl=getjitsihandle(true)
  if $jitsihdl=0 Then
	  updateinfo("No Jitsi","",$COLOR_RED)
	  GUISetState(@SW_SHOW)
	  logger("no_jitsi ")
  Else
	logger("present ")
	Local $iState = WinGetState($jitsihdl)
	logger(" state:" & $istate& " ")

	; if Jitsi Meet minimized, restore and move to bottom of screen
    If BitAND($iState, $WIN_STATE_MINIMIZED) Then
		logger("enabling ")
		WinSetState($jitsihdl,"",@SW_RESTORE)
		WinMove($jitsihdl,"",200,@desktopheight-60)
	EndIf

	; query screensharing mode
	local $rchdl=WinGetHandle("Screen Sharing Tracker","")
	if @error Then
		logger("no_sst ")
		updateinfo("RC ready","",$COLOR_GREEN)
		GUISetState(@SW_SHOW)
	Else
		GUISetState(@SW_HIDE)
	EndIf
	logger($rchdl & @crlf,99)

	; if mic configured, query mic mode and adjust Jitsi Meet if required
	if $confmic<>"" Then
		local $micoff=getbuttonstatus($_appstr_toolb_microphonemute)
		if ($confmic="off" and $micoff=0) OR ($confmic="on" and $micoff=1) Then
			logger("CJ: MICOFF is " & $micoff & ", config wants " & $confmic & " - pushing button" & @crlf,1)
			pushbutton($_appstr_toolb_microphonemute)
		EndIf
	EndIf
	; if cam configured, query cam mode and adjust Jitsi Meet if required
	if $confcam<>"" Then
		local $camoff=getbuttonstatus($_appstr_toolb_videomute)
		if ($confcam="off" and $camoff=0) OR ($confcam="on" and $camoff=1) Then
			logger("CJ: CAMOFF is " & $camoff & ", config wants " & $confcam & " - pushing button" & @crlf,1)
			pushbutton($_appstr_toolb_videomute)
		EndIf
	EndIf

	local $shareon=getbuttonstatus($_appstr_toolb_screenshareon)
	if ($shareon=0) Then
		logger("CJ: SHARING is " & $shareon & " - pushing button" & @crlf,1)
		pushbutton($_appstr_toolb_screenshareon)
	EndIf


  EndIf
  logger(@crlf)
endfunc

func getjitsihandle($mainjitsi)
	Local $aList = WinList("Jitsi Meet")
	local $i, $aExStyle;, $aStyle
	local $hdl=0
	For $i = 1 To $aList[0][0]
		If $aList[$i][0]<>"" then
			; $aStyle = _WinAPI_GetWindowLong($aList[$i][1],$GWL_STYLE)
			$aExStyle = _WinAPI_GetWindowLong($aList[$i][1],$GWL_EXSTYLE)
			if $aExStyle=256 And $mainjitsi=true then return $aList[$i][1]
			if $aExStyle=264 And $mainjitsi=false then return $aList[$i][1]
			;logger("gjh: " & $aList[$i][0] & "/" &  $aList[$i][1] & "/" & number($aList[$i][1],2) & " " & $i & " s:" & $aStyle & " x:" & $aExStyle & @crlf)
		EndIf
	Next
	return $hdl
EndFunc


func permitremotecontrol($justchecking)
	logger(@crlf & "*** permitremotecontrol ***" & @crlf)
	;WinActivate ( "Jitsi Meet")

	logger( "--- Find window/control: Jitsi Meet ---" & @CRLF )

	Local $pCondition0
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, "Jitsi Meet", $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF )
	logger( "$pCondition0 OK" & @CRLF )

	Local $pPane1, $oPane1
	$oDesktop.FindFirst( $TreeScope_Children, $pCondition0, $pPane1 )
	$oPane1 = ObjCreateInterface( $pPane1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oPane1 ) Then Return logger( "$oPane1 ERR" & @CRLF )
	logger( "$oPane1 OK" & @CRLF )


	logger( "--- Find window/control: " & $_appstr_rc_permit & " ---" & @CRLF )
	Local $pButton1, $oButton1
	logger("waiting for $oButton1" & @crlf)
	While Not IsObj($oButton1)
		$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, $_appstr_rc_permit, $pCondition0 )
		If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF )
		$oPane1.FindFirst( $TreeScope_Descendants, $pCondition0, $pButton1 )
		$oButton1 = ObjCreateInterface( $pButton1, $sIID_IUIAutomationElement, $dtag_IUIAutomationElement )
		If Not IsObj( $oButton1 ) Then
			if $justchecking=True then
				return(99)
			Else
				sleep(500)
			EndIf
		EndIf
	WEnd
	logger( "$oButton1 OK" & @CRLF )
	GUISetState(@SW_SHOW)
	updateinfo("RC permit","",$COLOR_SKYBLUE)
	logger("PR: Found remote control request" & @crlf,1)


	logger( "--- Invoke: Erlauben ---" & @CRLF )

	Local $pInvokePattern1, $oInvokePattern1
	$oButton1.GetCurrentPattern( $UIA_InvokePatternId, $pInvokePattern1 )
	$oInvokePattern1 = ObjCreateInterface( $pInvokePattern1, $sIID_IUIAutomationInvokePattern, $dtag_IUIAutomationInvokePattern )
	If Not IsObj( $oInvokePattern1 ) Then Return logger( "$oInvokePattern1 ERR" & @CRLF ,1)
	logger( "$oInvokePattern1 OK" & @CRLF )

	logger( "--- Invoke Pattern (action) Methods ---" & @CRLF )
	$oInvokePattern1.Invoke()
	logger( "$oInvokePattern1.Invoke()" & @CRLF )
	logger("PR: pressed " &$_appstr_rc_permit & " button" & @crlf,1)
	msleep(2000)
EndFunc



func getbuttonstatus($buttontext)
	logger( "--- Find window/control: Jitsi Meet ---" & @CRLF )

	Local $pCondition0
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, "Jitsi Meet", $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF )
	logger( "$pCondition0 OK" & @CRLF )

	Local $pPane1, $oPane1
	$oDesktop.FindFirst( $TreeScope_Children, $pCondition0, $pPane1 )
	$oPane1 = ObjCreateInterface( $pPane1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oPane1 ) Then Return logger( "$oPane1 ERR" & @CRLF )
	logger( "$oPane1 OK" & @CRLF )


	logger( "--- Find window/control: " & $buttontext & " ---" & @CRLF )
	Local $pButton1, $oButton1
	logger("waiting for $oButton1" & @crlf)
	While Not IsObj($oButton1)
		$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, $buttontext, $pCondition0 )
		If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF )
		$oPane1.FindFirst( $TreeScope_Descendants, $pCondition0, $pButton1 )
		$oButton1 = ObjCreateInterface( $pButton1, $sIID_IUIAutomationElement, $dtag_IUIAutomationElement )
		If Not IsObj( $oButton1 ) Then
			return(255)
		EndIf
	WEnd
	logger( "$oButton1 OK" & @CRLF )

	; --- Control Pattern Properties ---
	logger( "--- Control Pattern Properties ---" & @CRLF )

	Local $iLegacyIAccessibleState1
	$oButton1.GetCurrentPropertyValue( $UIA_LegacyIAccessibleStatePropertyId, $iLegacyIAccessibleState1 )
	logger( "$iLegacyIAccessibleState1 = " & $iLegacyIAccessibleState1 & @CRLF )
	return (BitAND($iLegacyIAccessibleState1,8)>0)*1


EndFunc


func pushbutton($buttontext)
	logger( "--- Find window/control: Jitsi Meet ---" & @CRLF )

	Local $pCondition0
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, "Jitsi Meet", $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF )
	logger( "$pCondition0 OK" & @CRLF )

	Local $pPane1, $oPane1
	$oDesktop.FindFirst( $TreeScope_Children, $pCondition0, $pPane1 )
	$oPane1 = ObjCreateInterface( $pPane1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oPane1 ) Then Return logger( "$oPane1 ERR" & @CRLF )
	logger( "$oPane1 OK" & @CRLF )


	logger( "--- Find window/control: " & $buttontext & " ---" & @CRLF )
	Local $pButton1, $oButton1
	logger("waiting for $oButton1" & @crlf)
	While Not IsObj($oButton1)
		$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, $buttontext, $pCondition0 )
		If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF ,1)
		$oPane1.FindFirst( $TreeScope_Descendants, $pCondition0, $pButton1 )
		$oButton1 = ObjCreateInterface( $pButton1, $sIID_IUIAutomationElement, $dtag_IUIAutomationElement )
		If Not IsObj( $oButton1 ) Then
			return(255)
		EndIf
	WEnd
	logger( "$oButton1 OK" & @CRLF )

	logger( "--- Invoke: " & $buttontext & " ---" & @CRLF )
    Local $pInvokePattern1, $oInvokePattern1
	$oButton1.GetCurrentPattern( $UIA_InvokePatternId, $pInvokePattern1 )
	$oInvokePattern1 = ObjCreateInterface( $pInvokePattern1, $sIID_IUIAutomationInvokePattern, $dtag_IUIAutomationInvokePattern )
	If Not IsObj( $oInvokePattern1 ) Then Return logger( "$oInvokePattern1 ERR" & @CRLF )
	logger( "$oInvokePattern1 OK" & @CRLF )

	logger( "--- Invoke Pattern (action) Methods ---" & @CRLF )
	$oInvokePattern1.Invoke()
	logger( "$oInvokePattern1.Invoke()" & @CRLF )
	logger("PB: button pressed: " & $buttontext & @crlf,1)

EndFunc




func configurescreensharing($justchecking)
	; ---------------- TEILEN
	logger(@crlf & "*** configurescreensharing ***" & @crlf)
	;WinActivate ( "Jitsi Meet")
    ; Create UI Automation object
    ;Local $oUIAutomation = ObjCreateInterface( $sCLSID_CUIAutomation8, $sIID_IUIAutomation3, $dtag_IUIAutomation3 )
    ;If Not IsObj( $oUIAutomation ) Then Return logger( "$oUIAutomation ERR" & @CRLF )
    ;logger( "$oUIAutomation OK" & @CRLF )

    ; Get Desktop element
    ;Local $pDesktop, $oDesktop
    ;$oUIAutomation.GetRootElement( $pDesktop )
    ;$oDesktop = ObjCreateInterface( $pDesktop, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
    ;If Not IsObj( $oDesktop ) Then Return logger( "$oDesktop ERR" & @CRLF )
    ;logger( "$oDesktop OK" & @CRLF )

	logger( "--- Find window/control: Jitsi Meet ---" & @CRLF )

	Local $pCondition0
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, "Jitsi Meet", $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF )
	logger( "$pCondition0 OK" & @CRLF )

	Local $pPane1, $oPane1
	$oDesktop.FindFirst( $TreeScope_Children, $pCondition0, $pPane1 )
	$oPane1 = ObjCreateInterface( $pPane1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oPane1 ) Then Return logger( "$oPane1 ERR" & @CRLF )
	logger( "$oPane1 OK" & @CRLF )

	logger( "--- Find window/control: " & $_appstr_rc_share & " ---" & @CRLF )

    Local $pButton2, $oButton2
	logger("waiting for $oButton2" & @crlf)
	While Not IsObj($oButton2)
		$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, $_appstr_rc_share, $pCondition0 )
		If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF )
		$oPane1.FindFirst( $TreeScope_Descendants, $pCondition0, $pButton2 )
		$oButton2 = ObjCreateInterface( $pButton2, $sIID_IUIAutomationElement, $dtag_IUIAutomationElement )
		If Not IsObj( $oButton2 ) Then
			if $justchecking=True then
				return(99)
			Else
				sleep(500)
			EndIf
		EndIf
	WEnd
	GUISetState(@SW_SHOW)
	updateinfo("RC configure","",$COLOR_SKYBLUE)
	logger( "$oButton2 OK" & @CRLF )

	logger( "--- Invoke: Teilen ---" & @CRLF )
    Local $pInvokePattern1, $oInvokePattern1
	$oButton2.GetCurrentPattern( $UIA_InvokePatternId, $pInvokePattern1 )
	$oInvokePattern1 = ObjCreateInterface( $pInvokePattern1, $sIID_IUIAutomationInvokePattern, $dtag_IUIAutomationInvokePattern )
	If Not IsObj( $oInvokePattern1 ) Then Return logger( "$oInvokePattern1 ERR" & @CRLF )
	logger( "$oInvokePattern1 OK" & @CRLF )

	logger( "--- Invoke Pattern (action) Methods ---" & @CRLF )
	$oInvokePattern1.Invoke()
	logger( "$oInvokePattern1.Invoke()" & @CRLF )

	msleep(3000)
	clearscreen()
	winsetstate("Jitsi Meet","",@SW_MINIMIZE)
endfunc


Func clearscreen()
	;WinActivate ( "Jitsi Meet")
	winsetstate("Screen Sharing Tracker","",@SW_HIDE)
	;winsetstate("Jitsi Meet","",@SW_MINIMIZE)
EndFunc

func checkcommand()
	local $cmd=iniread($commandfile,"command","run","*")
	local $jitsihdl
	if $cmd="*" then return
	inidelete($commandfile,"command","run")
	logger("COMMAND: " & $cmd & @crlf)
	if $cmd="startroom" Then
		local $cmdopt=iniread($commandfile,"command","startroom","*")
		if $cmdopt<>"*" Then
			iniwrite($commandfile,"latest","ran",$cmd&";"&$cmdopt)
			inidelete($commandfile,"command","startroom")
			$jitsihdl=getjitsihandle(true)
			if $jitsihdl=0 Then
					updateinfo("Start Jitsi","",$COLOR_BLUE)
					msleep(5000)
					run($jitsipath,"",@SW_SHOW)
			EndIf
			updateinfo("Start Conf","",$COLOR_BLUE)
			msleep(10000)
			logger("CK: command startroom "&$cmdopt&" received"& @crlf,1)
			startconference($cmdopt)
		EndIf
	EndIf
	if $cmd="killjitsi" Then
		logger("CK: command killjitsi received"& @crlf,1)
		iniwrite($commandfile,"latest","ran",$cmd)
		$jitsihdl=winexists("Jitsi Meet")
		logger("CK: killjitsi " & $jitsihdl &  @CRLF,5)
		if $jitsihdl<>0 Then
			GUISetState(@SW_SHOW)
			updateinfo("QUIT Conf","","000000")
			msleep(15000)
			$cmd=iniread($commandfile,"command","run","*")
			if $cmd="*" Then
				$jitsihdl=processexists("Jitsi Meet.exe")
				;$jitsihdl=winexists("Jitsi Meet")
				while $jitsihdl<>0
					logger("CK: killjitsi Jitsi closing: " & $jitsihdl & " ",5)
					processclose($jitsihdl)
					;WinKill($jitsihdl)
					logger(@error & @crlf,5)
					$jitsihdl=processexists("Jitsi Meet.exe")
					;$jitsihdl=winexists("Jitsi Meet")
				WEnd
			Else
				logger("CK: command killjitsi aborted " & $cmd &  @crlf,1)
			EndIf
		Else
			logger("CK: command killjitsi: no process found" & @CRLF,1)
		EndIf
	EndIf


EndFunc



Func enterconference($justchecking)
	logger(@crlf & "*** FUNC enterconference ***" & @crlf,3)

	;WinActivate ( "Jitsi Meet")

    ; Create UI Automation object
    ;Local $oUIAutomation = ObjCreateInterface( $sCLSID_CUIAutomation8, $sIID_IUIAutomation3, $dtag_IUIAutomation3 )
    ;If Not IsObj( $oUIAutomation ) Then Return logger( "$oUIAutomation ERR" & @CRLF )
    ;logger( "$oUIAutomation OK" & @CRLF )

    ; Get Desktop element
    ;Local $pDesktop, $oDesktop
    ;$oUIAutomation.GetRootElement( $pDesktop )
    ;$oDesktop = ObjCreateInterface( $pDesktop, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
    ;If Not IsObj( $oDesktop ) Then Return logger( "$oDesktop ERR" & @CRLF )
    ;logger( "$oDesktop OK" & @CRLF )



	logger( "--- Find window/control: Jitsi Meet ---" & @CRLF,3 )

	Local $pCondition0
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, "Jitsi Meet", $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF ,3)
	logger( "$pCondition0 OK" & @CRLF ,3)

	Local $pPane1, $oPane1
	$oDesktop.FindFirst( $TreeScope_Children, $pCondition0, $pPane1 )
	$oPane1 = ObjCreateInterface( $pPane1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oPane1 ) Then Return logger( "$oPane1 ERR" & @CRLF ,3)
	logger( "$oPane1 OK" & @CRLF,3 )

	; --- Find window/control ---

	logger( "--- Find window/control: Document ---" & @CRLF,3 )


	$oUIAutomation.CreatePropertyCondition( $UIA_ControlTypePropertyId, $UIA_DocumentControlTypeId, $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF,3 )
	logger( "$pCondition0 OK" & @CRLF,3 )

	Local $pDocument1, $oDocument1
	$oPane1.FindFirst( $TreeScope_Descendants, $pCondition0, $pDocument1 )
	$oDocument1 = ObjCreateInterface( $pDocument1, $sIID_IUIAutomationElement, $dtag_IUIAutomationElement )
	If Not IsObj( $oDocument1 ) Then Return logger( "$oDocument1 ERR" & @CRLF,3 )
	logger( "$oDocument1 OK" & @CRLF,3 )

	Local $pCondition1, $pAndCondition1

	; --- Find window/control ---

	if $confuser<>"" then

		logger( "--- Find window/control: " & $_appstr_cf_confuser & " ---" & @CRLF,3 )

		$oUIAutomation.CreatePropertyCondition( $UIA_ControlTypePropertyId, $UIA_EditControlTypeId, $pCondition0 )
		$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, $_appstr_cf_confuser, $pCondition1 )
		$oUIAutomation.CreateAndCondition( $pCondition0, $pCondition1, $pAndCondition1 )
		If Not $pAndCondition1 Then Return logger( "$pAndCondition1 ERR" & @CRLF,3 )
		logger( "$pAndCondition1 OK" & @CRLF,3 )

		Local $pEdit1, $oEdit1
		$oPane1.FindFirst( $TreeScope_Descendants, $pAndCondition1, $pEdit1 )
		$oEdit1 = ObjCreateInterface( $pEdit1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
		If Not IsObj( $oEdit1 ) Then Return logger( "$oEdit1 ERR" & @CRLF ,3)
		logger( "$oEdit1 OK" & @CRLF,3 )

		Local $pValue, $oValue
		$oEdit1.GetCurrentPattern( $UIA_ValuePatternId, $pValue )
		$oValue = ObjCreateInterface( $pValue, $sIID_IUIAutomationValuePattern, $dtag_IUIAutomationValuePattern )
		If Not IsObj( $oValue ) Then Return logger( "$oValue ERR" & @CRLF ,3)
		logger( "$oValue OK" & @CRLF,3)
		$oValue.SetValue( $confuser )
		logger("EC: Entered user name: " & $confuser & @crlf,1)
	EndIf





	logger( "--- Find window/control: " & $_appstr_cf_enterconference & " ---" & @CRLF ,3)

	; --- Find window/control ---
	$oUIAutomation.CreatePropertyCondition( $UIA_ControlTypePropertyId, $UIA_ButtonControlTypeId, $pCondition0 )
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, $_appstr_cf_enterconference, $pCondition1 )
	$oUIAutomation.CreateAndCondition( $pCondition0, $pCondition1, $pAndCondition1 )
	If Not $pAndCondition1 Then Return logger( "$pAndCondition1 ERR" & @CRLF,3 )
	logger( "$pAndCondition1 OK" & @CRLF ,3)

	Local $pButton1, $oButton1
	logger("waiting for $oButton1" & @crlf,2)
	While Not IsObj($oButton1)
		$oDocument1.FindFirst( $TreeScope_Descendants, $pAndCondition1, $pButton1 )
		$oButton1 = ObjCreateInterface( $pButton1, $sIID_IUIAutomationElement, $dtag_IUIAutomationElement )
		If Not IsObj( $oButton1 ) Then
			if $justchecking=True then
				return(99)
			Else
				sleep(500)
			EndIf
		EndIf
	WEnd
	logger("EC: discovered enterconference screen" & @crlf)
	GUISetState(@SW_SHOW)
	logger( "$oButton1 OK" & @CRLF ,3)
	updateinfo("Jitsi Join","",$COLOR_GREEN)

	; --- Invoke Pattern (action) Object ---

	logger( "--- Invoke Pattern (action) Object ---" & @CRLF ,3)

	Local $pInvokePattern1, $oInvokePattern1
	$oButton1.GetCurrentPattern( $UIA_InvokePatternId, $pInvokePattern1 )
	$oInvokePattern1 = ObjCreateInterface( $pInvokePattern1, $sIID_IUIAutomationInvokePattern, $dtag_IUIAutomationInvokePattern )
	If Not IsObj( $oInvokePattern1 ) Then Return logger( "$oInvokePattern1 ERR" & @CRLF ,3)
	logger( "$oInvokePattern1 OK" & @CRLF ,3)

	; --- Invoke Pattern (action) Methods ---

	logger( "--- Invoke Pattern (action) Methods ---" & @CRLF ,3)

	$oInvokePattern1.Invoke()
	logger("$oInvokePattern1.Invoke()" & @CRLF ,3)
	logger("EC: Button pressed: Enter conference" & @crlf,1)

endfunc


Func startconference($confurl)
    ; Create UI Automation object
	updateinfo("Jitsi Start","","*")
	logger(@crlf & "SC: starting conference " & $confurl & @crlf,1)
	WinActivate ( "Jitsi Meet")
    ;Local $oUIAutomation = ObjCreateInterface( $sCLSID_CUIAutomation8, $sIID_IUIAutomation3, $dtag_IUIAutomation3 )
    ;If Not IsObj( $oUIAutomation ) Then Return logger( "$oUIAutomation ERR" & @CRLF )
    ;logger( "$oUIAutomation OK" & @CRLF )

    ; Get Desktop element
    ;Local $pDesktop, $oDesktop
    ;$oUIAutomation.GetRootElement( $pDesktop )
    ;$oDesktop = ObjCreateInterface( $pDesktop, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
    ;If Not IsObj( $oDesktop ) Then Return logger( "$oDesktop ERR" & @CRLF )
    ;logger( "$oDesktop OK" & @CRLF )



	logger( "--- Find window/control ---" & @CRLF )

	Local $pCondition0
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, "Jitsi Meet", $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF ,1)
	logger( "$pCondition0 OK" & @CRLF )

	Local $pPane1, $oPane1
	$oDesktop.FindFirst( $TreeScope_Children, $pCondition0, $pPane1 )
	$oPane1 = ObjCreateInterface( $pPane1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oPane1 ) Then Return logger( "$oPane1 ERR" & @CRLF,1 )
	logger( "$oPane1 OK" & @CRLF )

	logger( "--- Find window/control ---" & @CRLF )

	Local $pCondition1
	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, "Enter a name for your conference or a Jitsi URL", $pCondition1 )
	If Not $pCondition1 Then Return logger( "$pCondition1 ERR" & @CRLF,1 )
	logger( "$pCondition1 OK" & @CRLF )

	Local $pText1, $oText1
	$oPane1.FindFirst( $TreeScope_Descendants, $pCondition1, $pText1 )
	$oText1 = ObjCreateInterface( $pText1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oText1 ) Then Return logger( "$oText1 ERR" & @CRLF,1 )
	logger( "$oText1 OK" & @CRLF )

	; --- Find window/control ---

	logger( "--- Find window/control: Edit ---" & @CRLF )


	$oUIAutomation.CreatePropertyCondition( $UIA_ControlTypePropertyId, $UIA_EditControlTypeId, $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF ,1)
	logger( "$pCondition0 OK" & @CRLF )

	Local $pEdit1, $oEdit1
	$oPane1.FindFirst( $TreeScope_Descendants, $pCondition0, $pEdit1 )
	$oEdit1 = ObjCreateInterface( $pEdit1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oEdit1 ) Then Return logger( "$oEdit1 ERR" & @CRLF,1 )
	logger( "$oEdit1 OK" & @CRLF )

	Local $pValue, $oValue
	$oEdit1.GetCurrentPattern( $UIA_ValuePatternId, $pValue )
	$oValue = ObjCreateInterface( $pValue, $sIID_IUIAutomationValuePattern, $dtag_IUIAutomationValuePattern )
	If Not IsObj( $oValue ) Then Return logger( "$oValue ERR" & @CRLF )
	logger( "$oValue OK" & @CRLF )
	$oValue.SetValue( $confurl )
	logger("SC: URL entered: "&$confurl & @crlf,1)


	; --- Find window/control ---

	logger( "--- Find window/control: " & $_appstr_cf_go & " ---" & @CRLF )

	$oUIAutomation.CreatePropertyCondition( $UIA_NamePropertyId, $_appstr_cf_go, $pCondition0 )
	If Not $pCondition0 Then Return logger( "$pCondition0 ERR" & @CRLF ,1)
	logger( "$pCondition0 OK" & @CRLF )

	Local $pButton1, $oButton1
	$oPane1.FindFirst( $TreeScope_Descendants, $pCondition0, $pButton1 )
	$oButton1 = ObjCreateInterface( $pButton1, $sIID_IUIAutomationElement4, $dtag_IUIAutomationElement4 )
	If Not IsObj( $oButton1 ) Then Return logger( "$oButton1 ERR" & @CRLF,1 )
	logger( "$oButton1 OK" & @CRLF )

	; --- Invoke Pattern (action) Object ---

	logger( "--- Invoke Pattern (action) Object ---" & @CRLF )

	Local $pInvokePattern1, $oInvokePattern1
	$oButton1.GetCurrentPattern( $UIA_InvokePatternId, $pInvokePattern1 )
	$oInvokePattern1 = ObjCreateInterface( $pInvokePattern1, $sIID_IUIAutomationInvokePattern, $dtag_IUIAutomationInvokePattern )
	If Not IsObj( $oInvokePattern1 ) Then Return logger( "$oInvokePattern1 ERR" & @CRLF )
	logger( "$oInvokePattern1 OK" & @CRLF )

	; --- Invoke Pattern (action) Methods ---

	logger( "--- Invoke Pattern (action) Methods ---" & @CRLF )

	$oInvokePattern1.Invoke()
	logger( "$oInvokePattern1.Invoke()" & @CRLF )
	logger("SC: Button '" & $_appstr_cf_go & "' pressed" & @crlf,1)





EndFunc

func updateinfo($text,$sub,$color)
	logger("INFO: " & $text & " " & $sub & @crlf,3)
	if $text<>"*" Then
		GUICtrlSetData($label_1,$text)
	EndIf
	if $sub<>"*" Then
		GUICtrlSetData($label_2,$sub)
	EndIf
    if $color<>"*" Then
		GUISetBkColor($color, $Form1)
	EndIf
EndFunc

Func msleep($msecs)
	local $max=$msecs / 1000
	For $i = $max to 1 step -1
		updateinfo("*",$i,"*")
		sleep(1000)
	next
	updateinfo("*","","*")
Endfunc




func exiterror($errortext)
	logger( $errortext & @crlf)
	exit
EndFunc


func logger($text,$level=3)

if $level<=$debuglevel Then
	consolewrite($text)
EndIf
EndFunc