# autojitsi

Automating Jitsi's Electron App für Desktop Sharing and remote control.


This requires:
- Jitsi Electron App in version 22
- AutoIt
- AutoIt UIAIncludes.7z (src: https://www.autoitscript.com/forum/topic/201683-ui-automation-udfs/) placed in /Includes


Setup:
- copy files in a dedicated folder
- copy contents of UIAincludes.7z into subfolder "includes"
- modify autojitsi.au3 "global $jitsipath=" to reflect .EXE of jitsi meet electron 